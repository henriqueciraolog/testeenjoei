package br.com.hciraolo.testeenjoei

import br.com.hciraolo.testeenjoei.model.GetProductsResponse
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface EnjoeiApi {
    @GET("products/home")
    fun getProducts(@Query("page") page: Int?): Observable<GetProductsResponse>
}