package br.com.hciraolo.testeenjoei

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object EnjoeiServiceFactory {
    private var enjoeiApi: EnjoeiApi? = null

    fun getEnjoeiService() : EnjoeiApi {
        if (enjoeiApi == null) {
            enjoeiApi = createEnjoeiService(BuildConfig.URL)
        }

        return enjoeiApi as EnjoeiApi
    }

    private fun createEnjoeiService(URL: String): EnjoeiApi {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY


        val client = OkHttpClient.Builder()
                .connectTimeout(45, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                /*.addInterceptor(HeaderInterceptor())*/
                .addInterceptor(interceptor)
                .build()


        val gson = GsonBuilder().create()

        val retroBuilder = Retrofit.Builder()
                .baseUrl(URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)

        return retroBuilder.build().create<EnjoeiApi>(EnjoeiApi::class.java!!)
    }
}