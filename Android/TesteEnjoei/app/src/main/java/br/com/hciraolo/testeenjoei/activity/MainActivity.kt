package br.com.hciraolo.testeenjoei.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.hciraolo.testeenjoei.presenter.MainPresenter
import br.com.hciraolo.testeenjoei.R
import br.com.hciraolo.testeenjoei.core.base.BaseActivity
import br.com.hciraolo.testeenjoei.listener.EndlessRecyclerViewScrollListener
import br.com.hciraolo.testeenjoei.model.Product
import br.com.hciraolo.testeenjoei.adapter.ProductsRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainPresenter.IMainView, ProductsRecyclerViewAdapter.OnItemClickListener {

    private lateinit var mPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MainPresenter(this)
        mPresenter.start()
    }

    override fun initViews() {
        srlProductsRefresh.setOnRefreshListener { mPresenter.getProducts() }
        rcvProducts.layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        rcvProducts.setHasFixedSize(true)
        rcvProducts.addOnScrollListener(object : EndlessRecyclerViewScrollListener(rcvProducts.layoutManager as GridLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                if (mPresenter.hasMorePages()) mPresenter.getMoreProducts()
            }
        })

        bnvMenu.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_home -> {
                    srlProductsRefresh.visibility = View.VISIBLE
                    txvMenuSelected.visibility = View.GONE
                }
                else -> {
                    srlProductsRefresh.visibility = View.GONE
                    txvMenuSelected.visibility = View.VISIBLE
                    txvMenuSelected.text = it.title
                }
            }

            true
        }
    }

    override fun hideSwipeLoading() {
        srlProductsRefresh.isRefreshing = false
    }

    override fun loadItems(products: ArrayList<Product>) {
        rcvProducts.adapter = ProductsRecyclerViewAdapter(products, this)
    }

    override fun addMoreItems(products: ArrayList<Product>) {
        (rcvProducts.adapter as ProductsRecyclerViewAdapter).addItems(products)
    }

    override fun setPresenter(presenter: MainPresenter) {
        mPresenter = presenter
    }

    override fun onItemClick(product: Product) {
        val intent = Intent(this, ProductDetailActivity::class.java)
        intent.putExtra("product", product)
        startActivity(intent)
    }

    override fun showNoConnection() {
        llNoConn.visibility = View.VISIBLE
        rcvProducts.visibility = View.INVISIBLE
    }

    override fun hideNoConnection() {
        llNoConn.visibility = View.INVISIBLE
        rcvProducts.visibility = View.VISIBLE
    }
}
