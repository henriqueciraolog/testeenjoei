package br.com.hciraolo.testeenjoei.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import br.com.hciraolo.testeenjoei.GlideImageLoadingService
import br.com.hciraolo.testeenjoei.R
import br.com.hciraolo.testeenjoei.adapter.MainSliderAdapter
import br.com.hciraolo.testeenjoei.model.Product
import kotlinx.android.synthetic.main.activity_product_detail.*
import ss.com.bannerslider.Slider
import ss.com.bannerslider.adapters.PositionController
import ss.com.bannerslider.adapters.SliderRecyclerViewAdapter

class ProductDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        val product = intent.getSerializableExtra("product") as Product

        Slider.init(GlideImageLoadingService(this))
        val adapter = MainSliderAdapter(product.photos!!)
        sldProductImages.setAdapter(adapter)
        if (product.originalPrice!! > 0) txvProductOldPrice.text = String.format("R$ %.0f", product.originalPrice)
        else txvProductOldPrice.visibility = View.GONE
        if (product.publishedCommentsCount!! > 0) txvProductCommentCount.text = String.format("%d", product.publishedCommentsCount)
        else txvProductCommentCount.visibility = View.GONE

        txvProductNewPrice.text = String.format("R$ %.0f", product.price)

        txvProductTitle.text = product.title
        txvProductDescription.text = product.content

        txvDiscountInstallment.text = String.format("%d%% off em até 12x", product.discountPercentage)
    }
}
