package br.com.hciraolo.testeenjoei.adapter

import br.com.hciraolo.testeenjoei.model.ImageInfo
import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

class MainSliderAdapter(private val mDatabase: List<ImageInfo>) : SliderAdapter() {

    override fun getItemCount(): Int {
        return mDatabase.size
    }

    override fun onBindImageSlide(position: Int, viewHolder: ImageSlideViewHolder) {
        val imageInfo = mDatabase[position]

        viewHolder.bindImageSlide(String.format("https://res.cloudinary.com/demo/image/upload/w_%d,h_%d,c_%s,g_%s/%s.jpg", 360, 428, imageInfo.crop, imageInfo.gravity, imageInfo.publicId))
    }
}