package br.com.hciraolo.testeenjoei.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.hciraolo.testeenjoei.viewholder.ProductViewHolder
import br.com.hciraolo.testeenjoei.R
import br.com.hciraolo.testeenjoei.model.Product

class ProductsRecyclerViewAdapter(myDataset: MutableList<Product>, myListener: OnItemClickListener) : RecyclerView.Adapter<ProductViewHolder>() {
    private var mDataset: MutableList<Product> = myDataset
    private var mListener: OnItemClickListener = myListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_product, parent, false)
        return ProductViewHolder(v)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = mDataset[position]
        holder.onBind(product, mListener)
    }

    override fun getItemCount(): Int {
        return mDataset.size
    }

    fun addItems(database: MutableList<Product>) {
        mDataset.addAll(database)
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(product: Product)
    }
}