package br.com.hciraolo.testeenjoei.interactor

import android.util.Log
import br.com.hciraolo.testeenjoei.EnjoeiApi
import br.com.hciraolo.testeenjoei.EnjoeiServiceFactory
import br.com.hciraolo.testeenjoei.R
import br.com.hciraolo.testeenjoei.core.base.BaseInteractor
import br.com.hciraolo.testeenjoei.model.GetProductsResponse
import br.com.hciraolo.testeenjoei.model.Pagination
import br.com.hciraolo.testeenjoei.model.Product
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class ProductInteractor : BaseInteractor() {

    interface OnGetProductsListener {
        fun onGetProductsSuccess(products: ArrayList<Product>, pages: Pagination)
        fun onGetProductsError()
    }

    interface OnGetMoreProductsListener {
        fun onGetMoreProductsSuccess(products: ArrayList<Product>)
        fun onGetMoreProductsError()
    }

    fun getProducts(listener: OnGetProductsListener) {
        val enjoeiApi = EnjoeiServiceFactory.getEnjoeiService()

        enjoeiApi.getProducts(1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<GetProductsResponse>() {
                    override fun onCompleted() {

                    }

                    override fun onError(e: Throwable) {
                        Log.e("APP ERROR", "onError: $e")
                        listener.onGetProductsError()
                    }

                    override fun onNext(response: GetProductsResponse) {
                        listener.onGetProductsSuccess(response.products!!, response.pagination!!)
                    }
                })
    }

    fun getProducts(page: Int?, listener: OnGetMoreProductsListener) {
        val enjoeiApi = EnjoeiServiceFactory.getEnjoeiService()

        enjoeiApi.getProducts(page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<GetProductsResponse>() {
                    override fun onCompleted() {

                    }

                    override fun onError(e: Throwable) {
                        Log.e("APP ERROR", "onError: $e")
                        listener.onGetMoreProductsError()
                    }

                    override fun onNext(response: GetProductsResponse) {
                        listener.onGetMoreProductsSuccess(response.products!!)
                    }
                })
    }
}