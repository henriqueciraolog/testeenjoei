package br.com.hciraolo.testeenjoei.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetProductsResponse {

    @SerializedName("products")
    @Expose
    var products: ArrayList<Product>? = null
    @SerializedName("pagination")
    @Expose
    var pagination: Pagination? = null

}