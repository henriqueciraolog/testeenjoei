package br.com.hciraolo.testeenjoei.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ImageInfo : Serializable {

    @SerializedName("public_id")
    @Expose
    var publicId: String? = null
    @SerializedName("crop")
    @Expose
    var crop: String? = null
    @SerializedName("gravity")
    @Expose
    var gravity: String? = null

}