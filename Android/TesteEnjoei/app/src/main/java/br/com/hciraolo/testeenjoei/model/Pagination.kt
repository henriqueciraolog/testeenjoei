package br.com.hciraolo.testeenjoei.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Pagination : Serializable {

    @SerializedName("current_page")
    @Expose
    var currentPage: Int? = null
    @SerializedName("total_pages")
    @Expose
    var totalPages: Int? = null

}