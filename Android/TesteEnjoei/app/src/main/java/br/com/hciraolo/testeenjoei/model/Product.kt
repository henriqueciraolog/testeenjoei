package br.com.hciraolo.testeenjoei.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Product : Serializable {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("discount_percentage")
    @Expose
    var discountPercentage: Int? = null
    @SerializedName("photos")
    @Expose
    var photos: List<ImageInfo>? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("price")
    @Expose
    var price: Double? = null
    @SerializedName("original_price")
    @Expose
    var originalPrice: Double? = null
    @SerializedName("size")
    @Expose
    var size: Any? = null
    @SerializedName("likes_count")
    @Expose
    var likesCount: Int? = null
    @SerializedName("maximum_installment")
    @Expose
    var maximumInstallment: Int? = null
    @SerializedName("published_comments_count")
    @Expose
    var publishedCommentsCount: Int? = null
    @SerializedName("content")
    @Expose
    var content: String? = null
    @SerializedName("user")
    @Expose
    var user: User? = null

}