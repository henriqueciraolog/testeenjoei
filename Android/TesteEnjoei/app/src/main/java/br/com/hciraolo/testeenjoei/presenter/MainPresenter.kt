package br.com.hciraolo.testeenjoei.presenter

import br.com.hciraolo.testeenjoei.core.base.IBasePresenter
import br.com.hciraolo.testeenjoei.core.base.IBaseView
import br.com.hciraolo.testeenjoei.interactor.ProductInteractor
import br.com.hciraolo.testeenjoei.model.Pagination
import br.com.hciraolo.testeenjoei.model.Product

class MainPresenter(private val mView: IMainView) : IBasePresenter, ProductInteractor.OnGetProductsListener, ProductInteractor.OnGetMoreProductsListener {

    var mInteractor: ProductInteractor = ProductInteractor()
    var actualPage: Int = 0
    var maxPages: Int = 0

    init {
        mView.setPresenter(this)
    }

    override fun start() {
        mView.initViews()
        mView.showProgress()
        getProducts()
    }

    fun getProducts() {
        mInteractor.getProducts(this)
    }

    fun getMoreProducts() {
        actualPage += 1
        mInteractor.getProducts(actualPage, this)
    }

    fun hasMorePages(): Boolean {
        return actualPage < maxPages
    }

    override fun onGetProductsSuccess(products: ArrayList<Product>, pages: Pagination) {
        mView.hideNoConnection()
        mView.hideSwipeLoading()
        mView.hideProgress()

        actualPage = pages.currentPage!!
        maxPages = pages.totalPages!!

        mView.loadItems(products)
    }

    override fun onGetProductsError() {
        mView.hideNoConnection()
        mView.hideSwipeLoading()
        mView.hideProgress()

        mView.showNoConnection()
    }

    override fun onGetMoreProductsSuccess(products: ArrayList<Product>) {
        mView.addMoreItems(products)
    }

    override fun onGetMoreProductsError() {
        //Do nothing!
    }

    interface IMainView : IBaseView {
        fun setPresenter(presenter: MainPresenter)
        fun initViews()
        fun hideSwipeLoading()
        fun loadItems(products: ArrayList<Product>)
        fun addMoreItems(products: ArrayList<Product>)
    }
}
