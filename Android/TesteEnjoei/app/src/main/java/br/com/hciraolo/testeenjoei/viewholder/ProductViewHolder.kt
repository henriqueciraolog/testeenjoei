package br.com.hciraolo.testeenjoei.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.hciraolo.testeenjoei.core.extension.setUrlImage
import br.com.hciraolo.testeenjoei.model.Product
import br.com.hciraolo.testeenjoei.adapter.ProductsRecyclerViewAdapter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.layout_product.view.*


class ProductViewHolder internal constructor(v: View) : RecyclerView.ViewHolder(v) {
    fun onBind(product: Product, listener: ProductsRecyclerViewAdapter.OnItemClickListener) {

        itemView.setOnClickListener { listener.onItemClick(product) }

        val requestOptions = RequestOptions().transform(RoundedCorners(4))

        itemView.imgProductThumb.setUrlImage(String.format("https://res.cloudinary.com/demo/image/upload/w_%d,h_%d,c_%s,g_%s/%s.jpg", 165, 195, product.photos!![0].crop, product.photos!![0].gravity, product.photos!![0].publicId), requestOptions)

        itemView.txvProductName.text = product.title
        itemView.txvProductPrice.text = String.format("R$ %.0f", product.price)

        if (product.discountPercentage!! > 0) itemView.txvProductDiscount.text = String.format("%d%%",product.discountPercentage)
        else itemView.txvProductDiscount.visibility = View.GONE

        itemView.imgProductOwnerAvatar.setUrlImage(String.format("https://res.cloudinary.com/demo/image/upload/w_%d,h_%d,c_%s,g_%s/%s.jpg", 30, 30, product.user!!.avatar!!.crop, product.user!!.avatar!!.gravity, product.user!!.avatar!!.publicId))
        itemView.txvProductNumLikes.text = String.format("%d", product.likesCount!!)
    }
}