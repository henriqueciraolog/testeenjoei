package br.com.hciraolo.testeenjoei.core.base

interface IBasePresenter {
    fun start()
}
