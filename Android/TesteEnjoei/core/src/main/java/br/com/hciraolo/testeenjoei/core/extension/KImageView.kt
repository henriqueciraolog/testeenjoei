package br.com.hciraolo.testeenjoei.core.extension

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.Transformation
import com.bumptech.glide.request.RequestOptions

fun ImageView.setUrlImage(url: String) {
    Glide.with(context).load(url).into(this)
}

fun ImageView.setUrlImage(url: String, requestOptions: RequestOptions) {
    Glide.with(context).load(url).apply(requestOptions).into(this)
}
